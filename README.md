# ForestVille WebApplication

## Setup

- First, you need to have Node.js installed
- Then, to setup backend:
    ```
    cd backend
    # Edit ormconfig.template.json, replacing uppercase values with your PostgreSQL DB config, and save it as ormconfig.json
    npm install
    npm run watch &
    npm run start
    ```
    backend will be available at http://localhost:8000
- To setup frontend:
    ```
    cd frontend
    npm install
    npm start
    ```
    The browser should open up by itself

- If you want to propagate your database with the test data, there is a file `backend/src/db/migrations.sql` containing SQL inserts for your DB.
**Important** - run your backend first, before migration, since it will create necessary tables
