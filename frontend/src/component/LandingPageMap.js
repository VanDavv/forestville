import React from 'react'
import ReactMapboxGl, {Marker} from "react-mapbox-gl";
import { Icon } from 'antd';

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoibWljb3JpeCIsImEiOiJjazBqbGhjMTUwYjF5M25saWJ5ZHBpOTY1In0.UUUIHEM0rJ0z3-UUqdfUvw"
});

const coordinates = [
    [20.831451, 52.126955],
    [20.816345, 52.128852],
    [20.886383, 52.268578],
    [21.201553, 52.200032],
    [21.178207, 52.223173],
    [21.266785, 52.168457],
    [21.304550, 52.089633],
    [21.226959, 52.289163]
]
class LandingPageMap extends React.PureComponent {
    constructor(props) {
        super(props);
        
    }

    render() {
        return (
            <Map
                style="mapbox://styles/mapbox/streets-v9"
                containerStyle={{
                    height: "60vh",
                    width: "100%"
                }}
                center={[21.017532, 52.237049]}
                zoom={[9]}
            >
                {
                    coordinates.map(coords => (
                        <Marker
                            coordinates={coords}
                            anchor="bottom">
                            <Icon type="pushpin"
                            theme="twoTone"
                            twoToneColor="#1DA57A"
                             style={{
                                fontSize: '2.5em'
                            }} />
                        </Marker>
                    ))
                }
            </Map>
        )
    }
}

export default LandingPageMap;