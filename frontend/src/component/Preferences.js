import React from 'react'
import { Row, Col, Form , Input, Icon,Button, message} from "antd";
import { makeAction } from '../redux/actions/makeAction';
import {connect} from 'react-redux'
const Preferences = Form.create()(props => {
    const { getFieldDecorator } = props.form;
    const handleSubmit = e => {
        e.preventDefault()
        props.form.validateFields((err, data) => {
            if(!err){
                console.log(err, data)
                props.updateProfile({
                    data,
                    cb: success => {
                       
                        if(success){
                            message.success('Profile updated successfully')
                        }
                    }
                })
            }
        })
    }
    return (
      <div className="container">
        <Row>
            <Col span={12}>
        <h1>Preferences</h1>
        
      <Form layout="vertical" onSubmit={handleSubmit}>
      <Form.Item label="First name">
          {getFieldDecorator('firstName', {
              initialValue: props.user.firstName
            })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />,
          )}
        </Form.Item>
        <Form.Item label="Last name">
          {getFieldDecorator('lastName', {
              initialValue: props.user.lastName
            })(
            <Input
              prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />,
          )}
        </Form.Item>
        <Form.Item label="Email">
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'This cannot be blank' }],
            initialValue: props.user.email
          })(
            <Input
              prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
            />
          )}
        </Form.Item>
     
        
        <Form.Item>
          <Button type="primary" htmlType="submit" >
            Save
          </Button>
        </Form.Item>
      </Form>
 
      </Col>
      </Row>
      </div>
    )
})
const mapStateToProps = (state) => ({
    user: state.auth.user
})
const mapDispatchToProps = {
    updateProfile: makeAction('UPDATE_USER_PROFILE')
}
export default connect(mapStateToProps, mapDispatchToProps)(Preferences)