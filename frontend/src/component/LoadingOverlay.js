import React from 'react'
import { Icon } from 'antd';


export default ({active}) => (
    <div className={`loading-overlay ${active ? 'active' : ''}`}>
        <Icon type="loading"  />
    </div>
)