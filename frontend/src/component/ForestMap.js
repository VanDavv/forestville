import React from 'react'
import ReactMapboxGl, {GeoJSONLayer, Popup} from "react-mapbox-gl";

const Map = ReactMapboxGl({
    accessToken: "pk.eyJ1IjoibWljb3JpeCIsImEiOiJjazBqbGhjMTUwYjF5M25saWJ5ZHBpOTY1In0.UUUIHEM0rJ0z3-UUqdfUvw"
});

const geojson = {
    'type': 'FeatureCollection',
    'features': [
        {
            'type': 'Feature',
            'geometry': {
                'type': 'LineString',
                'coordinates': [
                    [
                        -77.01239,
                        38.91275
                    ],
                    [
                        -77.00405,
                        38.93800
                    ]
                ]
            }
        }
    ]
};

class ForestMap extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Map
                style="mapbox://styles/mapbox/streets-v9"
                containerStyle={{
                    height: "100vh",
                    width: "100%"
                }}
                center={this.props.center}
                zoom={[8]}
            >
                {
                    this.props.popups
                }
                <GeoJSONLayer
                    data={{
                        "type": "FeatureCollection",
                        "features": this.props.cords.map(item => ({
                            'type': 'Feature',
                            'id': item.id,
                            'geometry': {
                                'type': 'Polygon',
                                'coordinates': [item.cords]
                            }
                        }))
                    }}
                    fillPaint={{
                        "fill-color": "#1890ff",
                        "fill-opacity": 0.8
                    }}
                    fillOnClick={this.props.onPolygonClick}
                />
            </Map>
        )
    }
}

export default ForestMap;