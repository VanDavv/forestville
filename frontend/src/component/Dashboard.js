import React from 'react'

import {Card, Row, Col, Statistic, Button, Table, Skeleton, Spin} from "antd";
import "./Dashboard.css";
import EmptyDashboard from './images/emptyDashboard.png';
import StatusImg1 from './images/status1.png';
import StatusImg2 from './images/status2.png';
import StatusImg3 from './images/status3.png';
import StatusImg4 from './images/status4.png';
import StatusImg5 from './images/status5.png';
import {Link} from "react-router-dom";
import {makeAction} from "../redux/actions/makeAction";
import {ORDERS_FETCH, RANKING_FETCH} from "../redux/actions/actionTypes";
import {connect} from "react-redux";
import {orders, ranking} from "../redux/selectors/app";
import _ from 'lodash';

const columns = [
    {
        title: 'Amount of trees',
        dataIndex: 'amount',
    },
    {
        title: 'Location',
        dataIndex: 'address',
    },
    {
        title: 'Money donated',
        dataIndex: 'donated',
    },
]

function resolveBadgeImage(count) {
    switch (true) {
        case count > 20:
            return StatusImg5;
        case count > 15:
            return StatusImg4;
        case count > 10:
            return StatusImg3;
        case count > 5:
            return StatusImg2;
        case count > 0:
            return StatusImg1;
        default:
            return EmptyDashboard;
    }
}

class Dashboard extends React.Component {
    componentDidMount() {
        this.props.fetchOrders();
        this.props.fetchRanking();
    }

    render() {
        const flat = _.flatten(this.props.orders.data.map(order => order.orderItems))
        const groupped = _.values(_.groupBy(flat, 'itemUnit.area.id'));
        const data = groupped.map((group, key) => ({
            group,
            key: key,
            amount: _.sumBy(group, 'amount'),
            donated: _.sum(group.map(item => item.itemUnit.price * item.amount)),
            address: group[0].itemUnit.area.name,
        }));
        return (
            <Row style={{padding: 50}}>
                <Col span={10} style={{textAlign: 'center', paddingRight: 80, marginTop: 100}}>
                    {
                        !this.props.orders.fetched
                            ? <Spin style={{marginTop: '50%'}} size="large"/>
                            : <>
                                <Link to="/forestries">
                                    <img height={300} width={330} src={resolveBadgeImage(this.props.orders.data.length)}
                                         alt="empty"/>
                                </Link>
                                {
                                    this.props.orders.data.length === 0
                                        ? <>
                                            <h3>You have not planted any trees yet</h3>
                                            <h2>Click on the pot to start!</h2>
                                        </>
                                        : <>
                                            <h3 style={{marginTop: 20}}>Welcome back fellow forester!</h3>
                                            <h2>Click on the pot to start!</h2>
                                        </>
                                }
                            </>
                    }
                </Col>
                <Col span={14}>
                    <Row gutter={70}>
                        <Col span={12}>
                            <Card style={{width: '100%', boxShadow: '#eee 2px 2px 25px', textAlign: 'center'}}>
                                <Statistic
                                    title={
                                        <h3>Account</h3>
                                    }
                                    value={0}
                                    valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30, marginTop: 11}}
                                    suffix="PLN"
                                />
                                <Button style={{marginTop: 10}} type="primary">
                                    Recharge
                                </Button>
                            </Card>
                        </Col>
                        <Col span={12}>
                            <Card style={{width: '100%', boxShadow: '#eee 2px 2px 25px'}}
                                  className="statistics-dashboard-right">
                                <Statistic
                                    title={
                                        <h3 style={{display: 'inline-block'}}>Planted trees</h3>
                                    }
                                    value={_.sumBy(flat, 'amount')}
                                    valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30}}
                                    suffix={<span style={{opacity: 0}}>.</span>}
                                />
                                <Statistic
                                    title={
                                        <h3 style={{display: 'inline-block'}}>Position</h3>
                                    }
                                    value={this.props.ranking.ranking_pos || '--'}
                                    valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30}}
                                    suffix={
                                        <span class="ranking-city-wrapper">
                                <span>in</span>
                                <span>Poland</span>
                            </span>
                                    }
                                />
                            </Card>
                        </Col>
                    </Row>
                    <Row style={{marginTop: 100}}>
                        <Card style={{width: '100%', boxShadow: '#eee 2px 2px 25px'}}>
                            <Table
                                bordered={false}
                                columns={columns}
                                dataSource={data}
                                title={() => <h2>My Plants</h2>}
                                expandedRowRender={
                                    ({group}) => (
                                        <div style={{display: 'flex', flexWrap: "wrap", justifyContent: 'space-evenly', width: '100%'}}>
                                            {
                                                group.reduce(
                                                    (sum, item) => {
                                                        console.log(sum);
                                                        for (let i = 0; i < item.amount; i++) {
                                                            sum.push(item.itemUnit.item)
                                                        }
                                                        return sum;
                                                    },
                                                    []
                                                ).map((item, key) => (
                                                    <Card
                                                        key={key}
                                                        hoverable
                                                        style={{width: 150, marginBottom: 10}}
                                                        cover={<img height={215} alt="example"
                                                                    src={item.imageUrl}/>}
                                                    >
                                                        <Card.Meta title={item.name}/>
                                                    </Card>
                                                ))
                                            }
                                        </div>
                                    )
                                }
                            />
                        </Card>
                    </Row>
                </Col>
            </Row>
        );
    }
};


const mapStateToProps = state => ({
    orders: orders(state),
    ranking: ranking(state)
});

const mapDispatchToProps = {
    fetchOrders: makeAction(ORDERS_FETCH),
    fetchRanking: makeAction(RANKING_FETCH),
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Dashboard);