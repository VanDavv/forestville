import {all} from 'redux-saga/effects';
import appSaga from "./appSaga";
import authSaga from './authSaga'

export default function* index() {
    yield all([
        appSaga(),
        authSaga()
    ]);
}