import {call, put, takeEvery, takeLatest} from 'redux-saga/effects'
import * as Auth from '../../service/auth'
import {save} from '../../service/storage';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* login(action) {

    try {
        const user = yield call(Auth.login, action.payload.creds)
        yield put({type: "USER_AUTHENTICATED", payload: {user}})
        if (action.payload.cb)
            yield call(action.payload.cb, {user})
    } catch (error) {
        yield put({type: "LOGIN_FAILED", payload: {error}})
        if (action.payload.cb)
            yield call(action.payload.cb, {error})
    }
}

function* register(action) {
    try {
        const user = yield call(Auth.register, action.payload.creds)
        yield put({type: "USER_AUTHENTICATED", payload: {user}})

        if (action.payload.cb)
            yield call(action.payload.cb, {user})
    } catch (error) {
        yield put({type: "REGISTER_FAILED", payload: {error}})
        if (action.payload.cb)
            yield call(action.payload.cb, {error})
    }
}

function* refreshAccessToken(action) {
    try {
        const accessToken = yield call(Auth.refreshAccessToken, {
            refreshToken: sessionStorage.getItem('refreshToken')
        })
        yield put({type: "REFRESH_ACCESS_TOKEN_SUCCESS", payload: {accessToken}})
        yield call(save, 'access_token', accessToken)
        if (action.payload.cb)
            yield call(action.payload.cb, {accessToken})
    } catch (error) {
        yield put({type: "REFRESH_ACCESS_TOKEN_FAILED", payload: {error}})
        if (action.payload.cb)
            yield call(action.payload.cb, {error})
    }
}

function* getCurrentUser() {
    yield put({type: "LOADING", payload: {loading: true}})
    try {
        const currentUser = yield call(Auth.getCurrentUser)
        yield put({type: "USER_AUTHENTICATED", payload: {user: currentUser}})
    } catch (error) {
        yield put({type: "LOGOUT"})
    } finally {
        yield put({type: "LOADING", payload: {loading: false}})
    }
}
function* updateUserProfile(action) {
   try {
      const user = yield call(Auth.updateUserProfile, {
         data: action.payload.data
      })
      console.log(user)
      yield put({type: "UPDATE_USER_PROFILE_SUCCESS", payload:{user}})
      if(action.payload.cb)
        yield call(action.payload.cb, {user})
   } catch (error) {
      yield put({type: "UPDATE_USER_PROFILE_FAILED", payload: {error}})
      if(action.payload.cb)
        yield call(action.payload.cb, {error})
   }
}
function* logout(action){
   yield call(Auth.logout)
}

function* authSaga() {
  yield takeLatest("LOGIN_REQUESTED", login)
  yield takeLatest("REGISTER_REQUESTED", register)
  yield takeLatest("USER_AUTHENTICATED", refreshAccessToken)
  yield takeLatest("REFRESH_ACCESS_TOKEN_REQUESTED", refreshAccessToken)
  yield takeLatest("GET_CURRENT_USER", getCurrentUser)
  yield takeLatest("UPDATE_USER_PROFILE", updateUserProfile)
  yield takeLatest("LOGOUT", logout)
}

export default authSaga