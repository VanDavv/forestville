import {combineReducers} from "redux";
import {connectRouter} from 'connected-react-router';
import appReducer from "./appReducer";
import authReducer from "./authReducer";

export default history => combineReducers({
    app: appReducer,
    auth: authReducer,
    router: connectRouter(history),
});
