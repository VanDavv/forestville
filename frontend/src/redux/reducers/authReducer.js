import * as actionTypes from '../actions/actionTypes';

const DEFAULT_STATE = {
    accessToken: null,
    user: {},
    isLoggedIn: false,
    loading: false
}

const authReducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case actionTypes.USER_AUTHENTICATED: {
            return {
                ...state,
                user: action.payload.user,
                isLoggedIn: true
            }
        }
        case actionTypes.REFRESH_ACCESS_TOKEN_SUCCESS: {
            return {
                ...state,
                accessToken: action.payload.accessToken,
                isLoggedIn: true
            }
        }
        case actionTypes.LOADING: {
            return {
                ...state,
                loading: action.payload.loading
            }
        }
        case actionTypes.UPDATE_USER_PROFILE_SUCCESS: {
            return {
                ...state,
                user: {
                    ...state.user,
                    ...action.payload.user
                }
            }
        }
        case actionTypes.LOGOUT: {
            return {
                ...state,
                ...DEFAULT_STATE
            }
        }
        default: {
            return state;
        }
    }
};

export default authReducer;
