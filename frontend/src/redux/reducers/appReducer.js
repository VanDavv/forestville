import * as actionTypes from '../actions/actionTypes';

const DEFAULT_STATE = {
    selectedLocation: {},
    forestries: [],
    lastSuccessfulOrder: {initial: true},
    orders: {
        fetched: false,
        data: [],
    },
    ranking: {},
};

const appReducer = (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case actionTypes.SELECT_LOCATION: {
            return {
                ...state,
                selectedLocation: action.payload,
            }
        }
        case actionTypes.ORDERS_FETCH: {
            return {
                ...state,
                orders: DEFAULT_STATE.orders
            }
        }
        case actionTypes.ORDERS_FETCH_SUCCESS: {
            return {
                ...state,
                orders: {
                    fetched: true,
                    data: action.payload,
                },
            }
        }
        case actionTypes.ORDERS_FETCH_FAILED: {
            return {
                ...state,
                orders: {
                    fetched: true,
                    data: [],
                },
            }
        }
        case actionTypes.RANKING_FETCH_SUCCESS: {
            return {
                ...state,
                ranking: action.payload,
            }
        }
        case actionTypes.CHECK_PAYPAL_STATUS_SUCCESS: {
            return {
                ...state,
                lastSuccessfulOrder: action.payload,
            }
        }
        case actionTypes.FORESTRIES_FETCH_SUCCESS: {
            return {
                ...state,
                forestries: action.payload.map(item => ({
                    ...item,
                    shape_data: JSON.parse(item.shape_data),
                }))
            }
        }
        default: {
            return state;
        }
    }
};

export default appReducer;
