import {createSelector} from 'reselect';
import _ from 'lodash';

export const appBranch = state => state.app;

export const selectedLocation = createSelector(
    appBranch,
    app => app.selectedLocation
);

export const selectedLocationUnits = createSelector(
    selectedLocation,
    location => location.itemUnits
);

export const forestries = createSelector(
    appBranch,
    app => app.forestries
);

export const lastSuccessfulOrder = createSelector(
    appBranch,
    app => app.lastSuccessfulOrder
);

export const orders = createSelector(
    appBranch,
    app => app.orders
);

export const ranking = createSelector(
    appBranch,
    app => _.get(app, 'ranking.data[0]', {})
);

export const forestriesShapes = createSelector(
    forestries,
    list => list.map(item => item.shape_data)
);