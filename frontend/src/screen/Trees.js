import React, {Component} from 'react'
import {Col, Card, Row, List, Icon, Avatar, Progress, Button} from "antd";
import {selectedLocationUnits} from "../redux/selectors/app";
import {connect} from "react-redux";
import _ from 'lodash';
import {Redirect} from "react-router-dom";
import {makeAction} from "../redux/actions/makeAction";
import {CHECKOUT_TREES} from '../redux/actions/actionTypes';

const list = [{
    "gender": "male",
    "name": {"title": "mr", "first": "oliver", "last": "thomas"},
    "email": "oliver.thomas@example.com",
    "nat": "NZ"
}];

class Trees extends Component {

    constructor() {
        super();

        this.state = {
            selected: [],
            loading: false,
        }
    }

    render() {
        if(!this.props.availableItems || this.props.availableItems.length === 0) {
            return <Redirect to="/forestries"/>
        }
        return (
            <Row>
                <Col span={16}>
                    <div className="trees_screen">
                        {this.props.availableItems.map(item => (
                            <Card
                                key={item.id}
                                style={{width: 300}}
                                cover={
                                    <img
                                        alt="example"
                                        style={{height: 400}}
                                        src={item.item.imageUrl}
                                    />
                                }
                                actions={[
                                    <Button
                                        onClick={() => this.setState({selected: this.state.selected.concat(item)})}
                                        shape="circle"
                                        icon="plus"
                                        key="plus"
                                    />,
                                    <Button
                                        onClick={() => {
                                            const index = this.state.selected.indexOf(item);
                                            if (index !== -1) {
                                                const selected = [...this.state.selected];
                                                delete selected[index]
                                                this.setState({selected: selected.filter(item => !!item)})
                                            }
                                        }}
                                        shape="circle"
                                        icon="minus"
                                        key="minus"
                                    />,
                                ]}
                            >
                                <Card.Meta
                                    title={<h3 style={{textAlign: 'center'}}>{item.item.name}</h3>}
                                    description={<span style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'space-between'
                                    }}>
                                        <span><h3>Price: <b>{item.price}zł</b></h3></span>
                                        <span>Demand:
                                            <Progress
                                                style={{marginLeft: 10}}
                                                width={50}
                                                type="circle"
                                                strokeColor={{
                                                    '0%': '#1890ff',
                                                    '100%': '#1DA57A',
                                                }}
                                                percent={item.urgencyFactor * 100}
                                            />
                                        </span>
                                    </span>}
                                />
                            </Card>
                        ))}
                    </div>
                </Col>
                <Col span={8}>
                    <List
                        className="demo-loadmore-list"
                        itemLayout="horizontal"
                        dataSource={_.values(_.groupBy(this.state.selected, 'id'))}
                        header={<div>Purchase list</div>}
                        renderItem={itemGroup => {
                            const item = itemGroup[0]
                            return (
                                <List.Item
                                    key={item.id}
                                    actions={[<a key="list-loadmore-edit">remove</a>]}
                                >
                                    <List.Item.Meta
                                        avatar={
                                            <Avatar src={item.item.thumbnailImageUrl}/>
                                        }
                                        title={<a href="https://ant.design">{item.item.name}</a>}
                                    />
                                    <div>
                                        <i style={{marginRight: 10}}>{item.price}zł</i>
                                        x{itemGroup.length}
                                        <b style={{marginLeft: 10}}>{item.price * itemGroup.length}zł</b>
                                    </div>
                                </List.Item>
                            )
                        }}
                        footer={
                            <div>
                                <div style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'space-between',
                                    paddingLeft: 50,
                                    paddingRight: 100,
                                }}>
                                    <b>Total</b>
                                    <h3 style={{fontWeight: 700}}>{_.sumBy(this.state.selected, 'price')}zł</h3>
                                </div>
                                <div style={{padding: "0 50px", marginTop: 50}}>
                                    <Button size="large" type="primary" loading={this.state.loading} block disabled={this.state.selected.length === 0} onClick={() => {
                                        this.props.checkout(this.state.selected);
                                        this.setState({loading: true})
                                    }}>
                                        Checkout
                                    </Button>
                                </div>
                            </div>
                        }
                    />
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    availableItems: selectedLocationUnits(state)
})

const mapDispatchToProps = {
    checkout: makeAction(CHECKOUT_TREES)
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Trees);