import React from 'react'
import {Result, Button, Icon, Row} from 'antd';
import {lastSuccessfulOrder} from '../redux/selectors/app';
import {connect} from 'react-redux'
import {Redirect, withRouter} from 'react-router-dom'


const OrderSuccess = ({order, history}) => {
    if(order.initial) {
        return <Redirect to="/"/>
    }
    return (
        <Result
            status="success"
            title="Your order was placed successfuly - thank you!"
            subTitle={`Order number: ${order.receipt.id} - use this number if you'd like to contact us about any order-related issues`}
            extra={[
                <Button type="primary" key="console" onClick={() => history.push('/')}>
                    Visit Dashboard
                </Button>,
                <Button key="buy" onClick={() => history.push('/forestries')}>Buy Again</Button>,
            ]}
        />
    );
}

const mapStateToProps = state => ({
    order: lastSuccessfulOrder(state)
})

export default connect(
    mapStateToProps
)(withRouter(OrderSuccess));