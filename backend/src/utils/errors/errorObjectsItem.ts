﻿import {CustomError} from './customError'

export default {
  itemAlreadyExists: new CustomError("This user already exists", 409),
  couldntCreateItem: new CustomError("Couldn't create user", 500),
  itemDoesntExist: new CustomError("Item doesn't exist", 404),

}