import * as Passport from 'passport'
import * as LocalStrategy from 'passport-local'
import * as express from 'express'
import * as crypto from 'crypto'
import { UserBase } from '../../../db/entity/UserBase'
import { getUserByEmail,getUserByEmailSensitive,getUserByIdSensitive, getUserById, saveUser, createUser, addFarmer, addDonator, getUserByRefreshToken } from '../../../db/services/UserService'
import { hashPassword, isValid } from '../../../utils/hasher'
import errorObjectsUser from '../../../utils/errors/errorObjectsUser'
import JWT from './JWTPromise'
import { Farmer } from '../../../db/entity/Farmer'
import { Donator } from '../../../db/entity/Donator'
import { create } from 'domain'
import { saveFarmer } from '../../../db/services/FarmerService'

export const SignIn = async function (req: express.Request, res:express.Response,next:express.NextFunction) {
    const userObject = req.body.user;
    let user:UserBase = await getUserByEmailSensitive(userObject.email);
    if (!user) return next(errorObjectsUser.userDoesntExist);
    if (!isValid(req.body.user.password, user.passwordHash)) return next(errorObjectsUser.invalidUsernameOrPassword);
    res.locals.user=user;
    console.log(res.locals);
    return next();
}

export const SignUpFarmer = async function (req: express.Request, res:express.Response,next:express.NextFunction) {
    const userObject = req.body.user;
    let user: UserBase = await getUserByEmail(userObject.email);
    console.log(user);
      if (!user) {

        var hash: string = await hashPassword(userObject.password);
        user = await createUser({email:userObject.email,passwordHash:hash});
        user =await saveUser(user);
        var refreshToken = user.id.toString() + '.' + crypto.randomBytes(40).toString('hex');
        user.refreshToken=refreshToken;
        user =await saveUser(user);
        var farmer = new Farmer();
        await addFarmer(user,farmer);
        if (!user) return next(errorObjectsUser.couldntCreateUser);
        res.locals.user=user;
        res.locals.jwt = await generateAccessToken(user.id);
        return next();
      }
      return next(errorObjectsUser.userAlreadyExists);
}

export const SignUpDonator = async function (req: express.Request, res:express.Response,next:express.NextFunction) {
    const userObject = req.body.user;
    let user: UserBase = await getUserByEmail(userObject.email);
      if (!user) {
        var hash: string = await hashPassword(userObject.password);
        user = await createUser({email:userObject.email,passwordHash:hash});
        user =await saveUser(user);
        var refreshToken = user.id.toString() + '.' + crypto.randomBytes(40).toString('hex');
        user.refreshToken=refreshToken;
        user =await saveUser(user);
        var donator = new Donator();
        await addDonator(user,donator);
        if (!user) return next(errorObjectsUser.couldntCreateUser);
        res.locals.user=user;
        return next();
      }
      return next(errorObjectsUser.userAlreadyExists);
}

export const authLevel={
    GUEST:0,
    USER:1,
    ADMIN:2

}


export const isLoggedIn = function(minAccessLevel){
  return async function (req: express.Request, res: express.Response, next) {
      console.log("is logged");
    if(req.headers.access_token){
      try{var data = await JWT.verify((<string>req.headers.access_token));}catch(err){return next(errorObjectsUser.invalidAccessToken)}

      res.locals.user=await getUserById(<number>data['id']);
      next();
    }else{
      if(minAccessLevel>authLevel.GUEST)
        next(errorObjectsUser.invalidAccessToken)
      else 
        next();
    }
  }
}

export const refreshAccessToken = async function (req: express.Request, res:express.Response,next:express.NextFunction) {
    const refreshToken = req.body.refreshToken;
    let user: UserBase = await getUserByRefreshToken(refreshToken);
      if (!user) {
        return next(errorObjectsUser.invalidRefreshToken)
      }
      res.locals.user = user;
      res.locals.jwt = await generateAccessToken(user.id);
      return next();
}

export async function generateAccessToken(id:number){
  return await JWT.sign({id:id});
}

