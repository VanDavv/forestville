// import { Application, Request, Response } from "express";
// import { PaypalService } from "../../utils/PaypalService";
// import { getOrderId, createOrder, createOrderItem, saveOrder, getOrderPaypalId, updateOrder } from "../../db/services/OrderService";
// import { OrderBase } from "../../db/entity/OrderBase";
// import { OrderItem } from "../../db/entity/OrderItem";
// import { ItemUnit } from "../../db/entity/ItemUnit";
// import { getItemUnit } from "../../db/services/ItemUnitService";

// const paypalService: PaypalService = new PaypalService();

// async function startOrder(req: Request, res: Response) {
//     const orderData = req.body;
//     const order = await createOrder();
//     const newOrder = await saveOrder(order);
//     let total = 0;
//     for (var odIx in orderData) {
//         const line = orderData[odIx];
//         const itemUnit: ItemUnit = await getItemUnit(line.itemId);
//         if (itemUnit === undefined) {
//             continue;
//         }
//         const orderItem: OrderItem = new OrderItem();
//         orderItem.item = line.itemId;
//         orderItem.quantity = line.quantity;
//         orderItem.price = itemUnit.price;
//         orderItem.order = newOrder;
//         total += (orderItem.price*orderItem.quantity);
//         // createOrderItem(orderItem);
//     }

//     const body = await paypalService.createOrder(total, newOrder.id);
//     order.paypal_id = body.id;
//     order.paypal_status = body.status;
//     await updateOrder(order);
//     res.json(order);
// }

// async function getOrder(req: Request, res: Response) {
//     const orderId = parseInt(req.params.id);
//     const order: OrderBase = await getOrderId(orderId);

//     if (order.paypal_status === "CREATED") {
//         const paypalOrderData = await paypalService.getOrder(orderId);
//         if (paypalOrderData.status !== order.paypal_status) {
//             order.paypal_status = paypalOrderData.status;
//         }
//     }

//     res.json(order);
// }

// async function postOrderStatusComplete(req: Request, res: Response) {
//     const orderId = req.params.id;
//     const order: OrderBase = await getOrderPaypalId(orderId);

//     order.paypal_status = "COMPLETE";
//     const savedOrder = await saveOrder(order);
//     return res.json(savedOrder);
// }

// module.exports = (app: Application) => {
//     app.post('/order', startOrder);
// }