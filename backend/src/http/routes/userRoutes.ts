import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { NextFunction } from "connect";
import { getUserById, getAllUsers, getBestPlanters, updateUserById, getUsersRankningPosition } from "../../db/services/UserService";
import { isLoggedIn, authLevel } from "../middlewares/passport/authenticate";
import { getOrdersByUserId, getUserOrders } from "../../db/services/OrderService";
import { mustBeDoner } from "../middlewares/security/user";
import { UserBase } from "../../db/entity/UserBase";

async function findUserById(req:Request,res:Response,next:NextFunction){
    const user = await getUserById(Number(req.params.id));
    res.json({user});
}
// async function findAllUsers(req:Request,res:Response,next:NextFunction){
//     const users = await getAllUsers();
//     res.json({users});
// }

// async function findBestPlanters(req:Request,res:Response,next:NextFunction){
//     const users = await getBestPlanters(1);
//     res.json({users});
// }

async function getRankningPosition(req:Request,res:Response,next:NextFunction){
    const orders = await getUserOrders(res.locals.user.id)
    if(!orders)return res.json({id:-1});
    const data = await getUsersRankningPosition(res.locals.user.id);
    res.json({data});
}

async function updateMyProfile(req:Request,res:Response,next:NextFunction){
    const userData = req.body.user;
    if(userData.passwordHash)delete userData['passwordHash'];
    await updateUserById(res.locals.user.id,userData);
    res.json({user:await getUserById(res.locals.user.id)});
}

module.exports = (app:Application)=>{
    //app.get('/user/all',getAllUsers);
    app.get('/user/rankingposition',isLoggedIn(authLevel.USER),mustBeDoner,getRankningPosition)
    app.put('/user/updateprofile',isLoggedIn(authLevel.USER),updateMyProfile);
    app.get('/user/:id/order/:orderid',isLoggedIn(authLevel.GUEST),mustBeDoner,findUserById);
    app.get('/user/:id/order',isLoggedIn(authLevel.GUEST),mustBeDoner,findUserById);
    app.get('/user/:id',isLoggedIn(authLevel.GUEST),findUserById);
}