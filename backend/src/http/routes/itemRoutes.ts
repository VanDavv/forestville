import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { NextFunction } from "connect";
import { getAreaById } from "../../db/services/AreaService";
import { saveItem, getAllItems } from "../../db/services/ItemService";
import { isLoggedIn, authLevel } from "../middlewares/passport/authenticate";

async function addItem(req:Request,res:Response,next:NextFunction){
    const itemData = req.body.item;
    const item = await saveItem(itemData);
    return res.json(item);
}

async function getItems(req: Request, res: Response, next: NextFunction) {
    const items = await getAllItems();
    return res.send(items);
}

module.exports = (app:Application)=>{
    app.get('/item',isLoggedIn(authLevel.GUEST),getItems);
    app.post('/item',isLoggedIn(authLevel.ADMIN),addItem);
}