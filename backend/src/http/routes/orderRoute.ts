import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { NextFunction } from "connect";
import { getAreaById } from "../../db/services/AreaService";
import { saveItem, getAllItems, getItemById } from "../../db/services/ItemService";
import { isLoggedIn, authLevel } from "../middlewares/passport/authenticate";
import { Receipt, ReceiptStatus } from "../../db/entity/Receipt";
import errorObjectsItem from "../../utils/errors/errorObjectsItem";
import { Area } from "../../db/entity/Area";
import { ItemUnit } from "../../db/entity/ItemUnit";
import { getItemUnitById } from "../../db/services/ItemUnitService";
import errorObjectsGlobal from "../../utils/errors/errorObjectsGlobal";
import { Order, OrderStatus } from "../../db/entity/Order";
import { OrderItem } from "../../db/entity/OrderItem";
import { saveOrder, getOrdersByUserId, getOrderById } from "../../db/services/OrderService";
import { saveReceipt } from "../../db/services/ReceiptService";
import { PaypalService } from "../../utils/PaypalService";
import { mustBeDoner } from "../middlewares/security/user";

const paypalService: PaypalService = new PaypalService();

class OrderedItem{
    id:number
    quantity:number
}

async function createOrder(req:Request,res:Response,next:NextFunction){
    const itemTable:OrderedItem[] = req.body.itemTable;
    var areaId:number;
    const receipt = new Receipt();
    receipt.price=0;
    receipt.sumOfItems=0;
    receipt.vat=23;
    receipt.status=ReceiptStatus.TO_BE_PAID;
    var order:Order = new Order();
    order.status = OrderStatus.TO_DO;
    order.user = res.locals.user;
    order.receipt=receipt;
    const itemsToAddToOrder:OrderItem[] = [];
    for(const orderedItem of itemTable ){
        const itemUnit:ItemUnit = await getItemUnitById(orderedItem.id);
        if(!itemUnit) return next(errorObjectsItem.itemDoesntExist);
        if(!areaId){
            areaId = itemUnit.area.id;
        }else{
            if(areaId!==itemUnit.area.id)return next(errorObjectsGlobal.itemsFromDifferentAreas)
        }
        const orderItem = new OrderItem();
        orderItem.itemUnit = itemUnit;
        orderItem.amount = orderedItem.quantity;
        itemsToAddToOrder.push(orderItem);
        receipt.price+=Number(orderedItem.quantity)*Number(itemUnit.price);
        receipt.sumOfItems+=orderedItem.quantity;
    }
    order.orderItems=itemsToAddToOrder;
    order =await saveOrder(order);
    order.createdAt=new Date();
    const body = await paypalService.createOrder(order.receipt.price, order.id);
    console.log("=========================================")
    console.error(body);
    order.paypal_id = body.id;
    order.paypal_status = body.status;
    await saveOrder(order);

    return res.json(order);
}



async function getOrder(req: Request, res: Response) {
    const orderId = parseInt(req.params.id);
    const order: Order = await getOrderById(orderId);

    if (order.paypal_status === "CREATED") {
        const paypalOrderData = await paypalService.getOrder(orderId);
        if (paypalOrderData.status !== order.paypal_status) {
            order.paypal_status = paypalOrderData.status;
        }
    }

    res.json({order:order});
}

async function postOrderStatusComplete(req: Request, res: Response) {
    const orderId =Number(req.params.id);
    const order: Order = await getOrderById(orderId);

    order.paypal_status = "COMPLETE";
    const savedOrder = await saveOrder(order);
    return res.json({order:savedOrder});
}


async function getUserOrders(req:Request,res:Response,next:NextFunction){
    const orders = await getOrdersByUserId(res.locals.user.id);
    return res.json(orders);
}

module.exports = (app:Application)=>{
    //app.get('/orders',isLoggedIn(authLevel.ADMIN),getItems);
    app.post('/order',isLoggedIn(authLevel.USER),mustBeDoner,createOrder);
    app.get('/order',isLoggedIn(authLevel.USER),mustBeDoner,getUserOrders);
    app.get('/order/:id', getOrder);
    app.post('/paypal/:id/complete', postOrderStatusComplete);
}