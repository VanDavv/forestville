import "reflect-metadata";
import {createConnection, getConnectionOptions} from "typeorm";
import {UserBase} from "./db/entity/UserBase";
import { saveUser, getAllUsers } from "./db/services/UserService";
import * as express from 'express';
import bodyParser = require("body-parser");
import morgan = require("morgan");
import { Farmer } from "./db/entity/Farmer";
import { saveFarmer, getAllFarmers } from "./db/services/FarmerService";
import { CustomError } from "./utils/errors/customError";
var cors = require('cors')

const app: express.Application = express();

async function start(){
    const connectionOptions = await getConnectionOptions();
Object.assign(connectionOptions, {multipleStatements: true});
    createConnection(connectionOptions).then(async connection => {

        const users = await getAllUsers();
        console.log("Loaded users: ", users);
    
        app.use(bodyParser.urlencoded({extended:true}));
        app.use(bodyParser.json());
        app.use((req,res,next)=>{
            res.locals={};
            next();
        })
        //@ts-ignore
        morgan(app);
        setupRoutes(app);
        app.use((err,req,res,next)=>{
            if(err instanceof CustomError){
                return res.status(err.status).json({err:err})
            }
            console.log(err);
            return res.status(500).json({err:new CustomError("Internal Server Error",500)});
            
        })
        app.listen(process.env.port||8000,()=>{
            console.log(`app listening on port: ${process.env.port||8000}`);
        })
    
    
    }).catch(error => console.log(error));
}



function setupRoutes(app:express.Application){
    require("./http/routes/userRoutes")(app);
    require("./http/routes/authenticate")(app);
    require("./http/routes/rootRoute")(app);
    require("./http/routes/areaRoutes")(app);
    require("./http/routes/itemRoutes")(app);
    require("./http/routes/orderRoute")(app);
}


start();