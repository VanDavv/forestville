import {getRepository, DeleteResult} from 'typeorm'
import { Area } from '../entity/Area';

export async function saveArea(area:Area):Promise<Area>{
    return await getRepository(Area).save(area);
}

export async function getAreaById(id:number):Promise<Area>{
    return await getRepository(Area).findOne({id:id});
}

export async function getAreaWithUnitIds(id:number):Promise<Area>{
    return await getRepository(Area).findOne(id,{relations:["itemUnits"]});
}

export async function getAllAreas():Promise<Area[]>{
    return await getRepository(Area).find();
}

export async function deleteAreaById(id:number):Promise<DeleteResult>{
    return await getRepository(Area).delete(id);
}