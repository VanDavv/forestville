import {getRepository, DeleteResult, UpdateResult, EntityManager} from 'typeorm'
import { UserBase } from '../entity/UserBase';
import { Farmer } from '../entity/Farmer';
import { saveFarmer, getFarmerById } from './FarmerService';
import { Donator } from '../entity/Donator';
import { saveDonator } from './DonatorService';

export async function saveUser(user:UserBase):Promise<UserBase>{
    return await getRepository(UserBase).save(user);
}

export async function createUser(user:UserBase|object):Promise<UserBase>{
    return await getRepository(UserBase).create(user);
}

export async function getUserById(id:number):Promise<UserBase>{
    return await getRepository(UserBase).findOne(id,{relations:["farmer","donator"]});
}

export async function getAllUsers():Promise<UserBase[]>{
    return await getRepository(UserBase).find();
}

export async function deleteUserById(id:number):Promise<DeleteResult>{
    return await getRepository(UserBase).delete(id);
}

export async function updateUserById(id:number,data:UserBase):Promise<UpdateResult>{
    return await getRepository(UserBase).update(id,data);
}

export async function getUserByEmail(email:string):Promise<UserBase>{
    return await getRepository(UserBase).findOne({email:email});
} 

export async function getUsersRankningPosition(id:number):Promise<UserBase>{
    const obj = await getRepository(UserBase).query(`select * from (select row_number() over() as ranking_pos,sum(order_item.amount) as total,"order"."userId" from "order" left join "order_item" ON "order_item"."orderId"="order"."id" GROUP by "order"."userId" order By "total" DESC
        )as counts where "counts"."userId"=${id.toString()}`);
        console.log(obj);
        return obj;
} 

export async function getUserByIdSensitive(id:number):Promise<UserBase>{
    return await getRepository(UserBase).createQueryBuilder("userBase")
    .addSelect("userBase.passwordHash")
    .addSelect("userBase.refreshToken")
    .where("userBase.id = :id", { id: id })
    .getOne();
}
export async function getUserByEmailSensitive(email:number):Promise<UserBase>{
    return await getRepository(UserBase).createQueryBuilder("userBase")
    .addSelect("userBase.passwordHash")
    .addSelect("userBase.refreshToken")
    .where("userBase.email = :email", { email: email })
    .getOne();
}



export async function addFarmer(user:UserBase,farmer:Farmer):Promise<UserBase>{
    farmer.user=user;
    farmer =await saveFarmer(farmer);
    farmer = await getFarmerById(farmer.id)
    user.farmer=farmer;
    return await saveUser(user);
}

export async function addDonator(user:UserBase,donator:Donator):Promise<UserBase>{
    user.donator = donator;
    await saveDonator(donator);
    return await saveUser(user);
}

export async function getDeepUser(id:number){
    return await getRepository(UserBase).findOne(id,{relations:["farmer","donator","farmer.areas"]});
}

export async function getUserByRefreshToken(refreshToken:string):Promise<UserBase>{
    return await getRepository(UserBase).findOne({refreshToken:refreshToken});
}

export async function getBestPlanters(id:number):Promise<UserBase[]>{
    //return await getRepository(Order).find({where:{user:{id:id}},relations:["orderItems","orderItems.itemUnit","orderItems.itemUnit.item","orderItems.itemUnit.area"]});
    return await getRepository(UserBase).createQueryBuilder("user")
    .select("COUNT(*)")
    .innerJoin("user.orders","order")
    .innerJoin("order.recipe","userBase")
    .innerJoin("order.orderItems","orderItem")
    .innerJoin("orderItem.itemUnit","itemUnit")
    .innerJoin("itemUnit.item","item")
    .innerJoin("itemUnit.area","area")
    .groupBy("user.id")
    .orderBy("1","DESC")
    .getMany()
}