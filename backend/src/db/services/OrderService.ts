import {getRepository, DeleteResult} from 'typeorm'
import { Order } from '../entity/Order';
import { UserBase } from '../entity/UserBase';

export async function saveOrder(order:Order):Promise<Order>{
    return await getRepository(Order).save(order);
}

export async function getOrderById(id:number):Promise<Order>{
    return await getRepository(Order).findOne({id:id});
}

export async function getAllOrders():Promise<Order[]>{
    return await getRepository(Order).find();
}

export async function deleteOrderById(id:number):Promise<DeleteResult>{
    return await getRepository(Order).delete(id);
}

export async function getOrdersByUserId(id:number):Promise<Order[]>{
    //return await getRepository(Order).find({where:{user:{id:id}},relations:["orderItems","orderItems.itemUnit","orderItems.itemUnit.item","orderItems.itemUnit.area"]});
    return await getRepository(Order).createQueryBuilder("order")
    .innerJoin("order.user","userBase")
    .where("userBase.id=:id",{id:id})
    .innerJoinAndSelect("order.orderItems","orderItem")
    .innerJoinAndSelect("orderItem.itemUnit","itemUnit")
    .innerJoinAndSelect("itemUnit.item","item")
    .innerJoinAndSelect("itemUnit.area","area").getMany()
}

export async function getUserOrders(userId:number):Promise<Order[]>{
    return getRepository(Order).find({where:{user:{id:userId}}})
}

