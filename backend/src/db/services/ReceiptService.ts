import {getRepository, DeleteResult} from 'typeorm'
import { Receipt } from '../entity/Receipt';

export async function saveReceipt(receipt:Receipt):Promise<Receipt>{
    return await getRepository(Receipt).save(receipt);
}

export async function getReceiptById(id:number):Promise<Receipt>{
    return await getRepository(Receipt).findOne({id:id});
}

export async function getAllReceipts():Promise<Receipt[]>{
    return await getRepository(Receipt).find();
}

export async function deleteReceiptById(id:number):Promise<DeleteResult>{
    return await getRepository(Receipt).delete(id);
}