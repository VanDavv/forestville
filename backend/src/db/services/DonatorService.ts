import {getRepository, DeleteResult} from 'typeorm'
import { Donator } from '../entity/Donator';

export async function saveDonator(donator:Donator):Promise<Donator>{
    return await getRepository(Donator).save(donator);
}

export async function getDonatorById(id:number):Promise<Donator>{
    return await getRepository(Donator).findOne({id:id});
}

export async function getAllDonators():Promise<Donator[]>{
    return await getRepository(Donator).find();
}

export async function deleteDonatorById(id:number):Promise<DeleteResult>{
    return await getRepository(Donator).delete(id);
}