TRUNCATE TABLE user_base RESTART IDENTITY CASCADE;
TRUNCATE TABLE farmer RESTART IDENTITY CASCADE;
TRUNCATE TABLE area RESTART IDENTITY CASCADE;
TRUNCATE TABLE item RESTART IDENTITY CASCADE;
TRUNCATE TABLE item_unit RESTART IDENTITY CASCADE;

insert into user_base("firstName", "email", "lastName", "avatarImageUrl", "passwordHash", "refreshToken")
values('Sławomir', 'ibl@ibles.waw.pl', 'Mydłowski', 'https://netka.gda.pl/wp-content/uploads/2018/03/fc79c10c-8378-4b90-bb44-9a3ef5349e80.jpg', '0', '0');

insert into farmer("description", "bannerImageUrl", "userId")
values('Nadleśniczy Nadleśnictwa Chojnów', 'https://www.ibles.pl/documents/17132/100083/wstega.jpg?t=1376122182880', 1);

insert into area("id", "longitude", "latitude", "shape_data", "name", "bannerImageUrl", "description", "address", "farmerId")
values(1, 21.012229, 52.229676, '[[20.856964, 52.113859],[20.837610, 52.106062],[20.843217, 52.099303],[20.845758, 52.092091],[20.859020, 52.087726],[20.861811, 52.090065], [20.881862, 52.093143], [20.886040, 52.103818], [20.868920, 52.106793], [20.866407, 52.112556], [20.856926, 52.113864]]',
      'Lasy Sękocińskie', 'https://lh5.googleusercontent.com/p/AF1QipOwJfyA_L-IDWH0aUhLsIn2SLZhbMD0HZod66iY=w408-h544-k-no', 'Kompleks leśny położony w województwie mazowieckim', 'Sękocin Stary, ul. Braci Leśnej nr 3', 1);

insert into area("id", "longitude", "latitude", "shape_data", "name", "bannerImageUrl", "description", "address", "farmerId")
values(2, 21.012229, 52.229676, '[[20.622526,52.345297],[20.592599,52.262614],[20.731721,52.287546],[20.874147,52.295428],[20.85836,52.340958],[20.751515,52.371184]]',
      'Kampinowska Szkółka Leśna', 'https://lh5.googleusercontent.com/p/AF1QipMxZM2tr_T4Qak4wvbhz9h02qIhTCpJAY1PEPY8=w408-h306-k-no', '38 544 hektary zalesionego parku z zabytkami oraz trasami rowerowymi, pieszymi i konnymi.', 'ul. Tetmajera 38, Izabelin', 1);

insert into area("id", "longitude", "latitude", "shape_data", "name", "bannerImageUrl", "description", "address", "farmerId")
values(3, 20.210946, 52.021799, '[[20.041316, 52.06592], [20.313238, 51.871647], [20.412668, 52.05506], [20.227846, 52.086435]]',
      'Bolimowska Szkółka Leśna', 'https://lh5.googleusercontent.com/p/AF1QipN_NuIrezKTCZQKEWkN2M7PJHsEsuhOpQlEmlfN=w426-h240-k-no', 'Lasy czekają na Ciebie!', 'ul. Bantona 38, Skierniewice', 1);

insert into area("id", "longitude", "latitude", "shape_data", "name", "bannerImageUrl", "description", "address", "farmerId")
values(4, 21.340213, 52.060931, '[[21.263135, 52.08756], [21.388740, 51.969616], [21.493090, 52.001965], [21.366531, 52.102160]]',
      'Mazowiecki Park Krajobrazowy', 'https://lh5.googleusercontent.com/p/AF1QipOVXa8j7RdRziyRMEI3CPOWxIpNE0Kzu9GOm5k=w408-h306-k-no', 'Lasy i jezioroczekają na Ciebie!', '05-400 Otwock', 1);

insert into item("name", "type", "imageUrl", "thumbnailImageUrl")
values('Pine tree', 'tree','https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg','https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg');

insert into item("name", "type", "imageUrl", "thumbnailImageUrl")
values('Oak tree', 'tree','https://indianoakstreefarm.com/wp-content/uploads/2017/08/white-oak-3.jpg','https://cdn.iconscout.com/icon/premium/png-256-thumb/oak-tree-13-915208.png');

insert into item("name", "type", "imageUrl", "thumbnailImageUrl")
values('Hickory tree', 'tree','https://upload.wikimedia.org/wikipedia/commons/e/eb/Carya_Morton_29-U-10.jpg','https://cdn.iconscout.com/icon/premium/png-256-thumb/bitter-nut-hickory-915230.png');

insert into item("name", "type", "imageUrl", "thumbnailImageUrl")
values('Spruce tree', 'tree','https://upload.wikimedia.org/wikipedia/commons/8/81/Picea_abies.jpg','https://cdn.iconscout.com/icon/premium/png-256-thumb/fluffy-young-branch-fir-tree-coniferous-tree-916101.png');

insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.7, 25.0, 2, 1);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.3, 20.0, 2, 2);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.3, 30.0, 2, 3);

insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.7, 35.0, 1, 1);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.6, 25.0, 1, 2);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.6, 30.0, 1, 3);

insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.2, 35.0, 3, 2);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.7, 15.0, 3, 4);

insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.2, 35.0, 4, 3);
insert into item_unit("urgencyFactor", "price", "areaId", "itemId") values(0.3, 15.0, 4, 4);