import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";

@Entity()
export class Donator{

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    account_balance:number
}
