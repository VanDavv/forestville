import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany, JoinColumn } from "typeorm";
import { UserBase } from "./UserBase";
import { OrderItem } from "./OrderItem";
import { Receipt } from "./Receipt";

@Entity()
export class Order {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type=>UserBase)
    @JoinColumn()
    user: UserBase;
    //TODO potential loop
    @OneToMany(type => OrderItem,orderItem=>orderItem.order,{eager:true,cascade:true})
    orderItems: OrderItem[];

    @OneToOne(type=>Receipt,{eager:true,cascade:true})
    @JoinColumn()
    receipt:Receipt

    @Column()
    status: string;

    @Column({nullable:true})
    paypal_status: string;

    @Column({nullable: true})
    paypal_id: string;

    @Column({nullable: true})
    createdAt: Date;
}

export const OrderStatus={
    TO_DO:"TO_DO",
    PENDING:"PENDING",
    DONE:"DONE"
}