import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from "typeorm";
import { Area } from "./Area";
import { Item } from "./Item";

@Entity()
export class ItemUnit {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type=>Item, {eager: true})
    @JoinColumn()
    item: Item;

    @ManyToOne(type=>Area)
    area: Area;

    @Column("double precision")
    urgencyFactor: number;

    @Column({default:0})
    price: number;
}